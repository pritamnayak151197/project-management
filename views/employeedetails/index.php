<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeedetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Employeedetails';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employeedetails-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Employeedetails', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'emp_details_id',
            'emp_id',
            'address',
            'po',
            'ps',
            //'dist',
            //'state',
            //'on_date',
            //'status',
            //'up_date',
            //'is_delete',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
