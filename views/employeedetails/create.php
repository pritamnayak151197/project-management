<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Employeedetails */

$this->title = 'Create Employeedetails';
$this->params['breadcrumbs'][] = ['label' => 'Employeedetails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employeedetails-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
