<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Employeedetails */

$this->title = 'Update Employeedetails: ' . $model->emp_details_id;
$this->params['breadcrumbs'][] = ['label' => 'Employeedetails', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->emp_details_id, 'url' => ['view', 'id' => $model->emp_details_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="employeedetails-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
