<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmployeedetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employeedetails-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'emp_details_id') ?>

    <?= $form->field($model, 'emp_id') ?>

    <?= $form->field($model, 'address') ?>

    <?= $form->field($model, 'po') ?>

    <?= $form->field($model, 'ps') ?>

    <?php // echo $form->field($model, 'dist') ?>

    <?php // echo $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'on_date') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'up_date') ?>

    <?php // echo $form->field($model, 'is_delete') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
