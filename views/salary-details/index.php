<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SalaryDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Salary Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="salary-details-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Salary Details', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'salary_id',
            'emp_id',
            'sal_ammount',
            'designation',
            'on_date',
            //'status',
            //'up_date',
            //'is_delete',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
