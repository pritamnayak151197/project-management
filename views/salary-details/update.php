<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SalaryDetails */

$this->title = 'Update Salary Details: ' . $model->salary_id;
$this->params['breadcrumbs'][] = ['label' => 'Salary Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->salary_id, 'url' => ['view', 'id' => $model->salary_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="salary-details-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
