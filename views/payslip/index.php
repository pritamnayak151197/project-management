<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PayslipSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payslips';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payslip-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Payslip', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'payslip_id',
            'emp_id',
            'salary_id',
            'sal_ammount',
            'month',
            //'date',
            //'on_date',
            //'status',
            //'up_date',
            //'is_delete',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
