<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Payslip */

$this->title = 'Update Payslip: ' . $model->payslip_id;
$this->params['breadcrumbs'][] = ['label' => 'Payslips', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->payslip_id, 'url' => ['view', 'id' => $model->payslip_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payslip-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
