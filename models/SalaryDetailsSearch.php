<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SalaryDetails;

/**
 * SalaryDetailsSearch represents the model behind the search form of `app\models\SalaryDetails`.
 */
class SalaryDetailsSearch extends SalaryDetails
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['salary_id', 'emp_id', 'status', 'is_delete'], 'integer'],
            [['sal_ammount', 'designation', 'on_date', 'up_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SalaryDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'salary_id' => $this->salary_id,
            'emp_id' => $this->emp_id,
            'on_date' => $this->on_date,
            'status' => $this->status,
            'up_date' => $this->up_date,
            'is_delete' => $this->is_delete,
        ]);

        $query->andFilterWhere(['like', 'sal_ammount', $this->sal_ammount])
            ->andFilterWhere(['like', 'designation', $this->designation]);

        return $dataProvider;
    }
}
