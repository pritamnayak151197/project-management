<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "salary_details".
 *
 * @property int $salary_id
 * @property int $emp_id
 * @property string $sal_ammount
 * @property string $designation
 * @property string $on_date
 * @property int $status
 * @property string $up_date
 * @property int $is_delete
 */
class SalaryDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'salary_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_id', 'sal_ammount', 'designation', 'on_date', 'up_date'], 'required'],
            [['emp_id', 'status', 'is_delete'], 'integer'],
            [['on_date', 'up_date'], 'safe'],
            [['sal_ammount', 'designation'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'salary_id' => 'Salary ID',
            'emp_id' => 'Emp ID',
            'sal_ammount' => 'Sal Ammount',
            'designation' => 'Designation',
            'on_date' => 'On Date',
            'status' => 'Status',
            'up_date' => 'Up Date',
            'is_delete' => 'Is Delete',
        ];
    }
}
