<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payslip".
 *
 * @property int $payslip_id
 * @property int $emp_id
 * @property int $salary_id
 * @property string $sal_ammount
 * @property string $month
 * @property string $date
 * @property string $on_date
 * @property int $status
 * @property string $up_date
 * @property int $is_delete
 */
class Payslip extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payslip';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_id', 'salary_id', 'sal_ammount', 'month', 'date', 'on_date', 'status', 'up_date', 'is_delete'], 'required'],
            [['emp_id', 'salary_id', 'status', 'is_delete'], 'integer'],
            [['month', 'date', 'on_date', 'up_date'], 'safe'],
            [['sal_ammount'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'payslip_id' => 'Payslip ID',
            'emp_id' => 'Emp ID',
            'salary_id' => 'Salary ID',
            'sal_ammount' => 'Sal Ammount',
            'month' => 'Month',
            'date' => 'Date',
            'on_date' => 'On Date',
            'status' => 'Status',
            'up_date' => 'Up Date',
            'is_delete' => 'Is Delete',
        ];
    }
}
