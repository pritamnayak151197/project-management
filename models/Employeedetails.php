<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employeedetails".
 *
 * @property int $emp_details_id
 * @property int $emp_id
 * @property string $address
 * @property string $po
 * @property string $ps
 * @property string $dist
 * @property string $state
 * @property string $on_date
 * @property int $status
 * @property string $up_date
 * @property int $is_delete
 */
class Employeedetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employeedetails';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_id', 'address', 'po', 'ps', 'dist', 'state', 'on_date', 'up_date'], 'required'],
            [['emp_id', 'status', 'is_delete'], 'integer'],
            [['on_date', 'up_date'], 'safe'],
            [['address', 'po', 'ps', 'dist', 'state'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'emp_details_id' => 'Emp Details ID',
            'emp_id' => 'Emp ID',
            'address' => 'Address',
            'po' => 'Po',
            'ps' => 'Ps',
            'dist' => 'Dist',
            'state' => 'State',
            'on_date' => 'On Date',
            'status' => 'Status',
            'up_date' => 'Up Date',
            'is_delete' => 'Is Delete',
        ];
    }
}
