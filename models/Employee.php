<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employee".
 *
 * @property int $emp_id
 * @property string $emp_name name of the employee
 * @property string $password
 * @property string $on_date
 * @property int $status
 * @property string $up_date
 * @property int $is_delete
 * @property string $created_by
 */
class Employee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_id', 'emp_name', 'password', 'on_date', 'up_date'], 'required'],
            [['emp_id', 'status', 'is_delete'], 'integer'],
            [['on_date', 'up_date'], 'safe'],
            [['emp_name', 'password'], 'string', 'max' => 50],
            [['created_by'], 'string', 'max' => 20],
            [['emp_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'emp_id' => 'Emp ID',
            'emp_name' => 'name of the employee',
            'password' => 'Password',
            'on_date' => 'On Date',
            'status' => 'Status',
            'up_date' => 'Up Date',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
        ];
    }
}
