<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Employeedetails;

/**
 * EmployeedetailsSearch represents the model behind the search form of `app\models\Employeedetails`.
 */
class EmployeedetailsSearch extends Employeedetails
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_details_id', 'emp_id', 'status', 'is_delete'], 'integer'],
            [['address', 'po', 'ps', 'dist', 'state', 'on_date', 'up_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Employeedetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'emp_details_id' => $this->emp_details_id,
            'emp_id' => $this->emp_id,
            'on_date' => $this->on_date,
            'status' => $this->status,
            'up_date' => $this->up_date,
            'is_delete' => $this->is_delete,
        ]);

        $query->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'po', $this->po])
            ->andFilterWhere(['like', 'ps', $this->ps])
            ->andFilterWhere(['like', 'dist', $this->dist])
            ->andFilterWhere(['like', 'state', $this->state]);

        return $dataProvider;
    }
}
